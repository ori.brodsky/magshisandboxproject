#include "pch.h"
#include <iostream>
#include <Windows.h>

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved) {
    switch (ul_reason_for_call) {
    case DLL_PROCESS_ATTACH:
        // Code to run when the DLL is loaded into a process
        std::cout << "Injected Code" << std::endl;
        break;

    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        // Code to run during DLL cleanup
        break;
    }

    return TRUE;
}