// ProcessHijacking.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <Windows.h>
#include <string>
#include <TlHelp32.h>

int getProcID(const wchar_t* procname) {

    PROCESSENTRY32 entry;
    entry.dwSize = sizeof(PROCESSENTRY32);

    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

    if (Process32First(snapshot, &entry) == TRUE)
    {
        while (Process32Next(snapshot, &entry) == TRUE)
        {
            if (wcscmp(entry.szExeFile, procname) == 0)
            {
                CloseHandle(snapshot);
                return entry.th32ProcessID;
            }
        }
    }
    return -1;

}

int main()
{
    const char* dllPath = "ProcessInjectionDLL.dll"; // Replace with the actual path to your DLL

    // Get the target process ID (you might want to adapt this to your specific case)

    DWORD targetProcessID = getProcID(L"Numbers.exe");
    std::cout << targetProcessID << std::endl;
    // Open the target process
    HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, targetProcessID);

    if (hProcess)
    {
        // Allocate memory in the target process to store the DLL path
        LPVOID dllPathRemote = VirtualAllocEx(hProcess, NULL, strlen(dllPath) + 1, MEM_COMMIT, PAGE_READWRITE);

        // Write the DLL path to the allocated memory in the target process
        WriteProcessMemory(hProcess, dllPathRemote, dllPath, strlen(dllPath) + 1, NULL);

        // Get the address of the LoadLibraryA function
        LPVOID loadLibraryAddr = (LPVOID)GetProcAddress(GetModuleHandleA("kernel32.dll"), "LoadLibraryA");

        if (!loadLibraryAddr) {
            std::cerr << "Failed to get the address of LoadLibraryA" << std::endl;
            // Handle error or return
        }

        // Create a remote thread to load the DLL
        HANDLE hThread = CreateRemoteThread(hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)loadLibraryAddr, dllPathRemote, 0, NULL);

        if (hThread)
        {
            // Wait for the thread to finish
            WaitForSingleObject(hThread, INFINITE);

            std::cout << "Code injected successfully!" << std::endl;

            // Clean up
            CloseHandle(hThread);
        }
        else
        {
            std::cerr << "Failed to create remote thread, Error: " << GetLastError() << std::endl;
        }

        // Clean up
        VirtualFreeEx(hProcess, dllPathRemote, 0, MEM_RELEASE);
        CloseHandle(hProcess);
    }
    else
    {
        std::cerr << "Failed to open target process" << std::endl;
    }

    return 0;
}
