import psutil
from scapy.all import sniff, TCP


def packet_callback(packet):
    if TCP in packet:
        payload = bytes(packet[TCP].payload)
        hex_payload = ' '.join([hex(byte)[2:].zfill(2) for byte in payload])
        print(hex_payload)
        # Add more conditions based on the specific file types you want to monitor (e.g., exe, dll)
        if b'.exe' in payload or b'.dll' in payload or b'.lib' in payload or b'.zip' in payload or b'PK\x03\x04' in payload:
            print(f"is downloading a file.")


def start_sniffing():
    print(b'.zip')
    sniff(prn=packet_callback, store=0)


if __name__ == "__main__":
    start_sniffing()
