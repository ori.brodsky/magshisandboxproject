from psutil import net_connections
from os import getpid
from scapy.all import sniff, conf
from scapy.layers.dns import DNS, DNSQR
from scapy.layers.inet import TCP, UDP, IP
import socket
DNS_PORT = 53


def get_pid(port):
    connections = net_connections()
    for con in connections:
        if con.raddr != tuple():
            if con.raddr.port == port:
                return con.pid, con.status
        if con.laddr != tuple():
            if con.laddr.port == port:
                return con.pid
    return 0


def dns_packet_callback(packet):
    blackbook = open('..\\NetworkAnalysis\\blackbook.txt', 'r')
    malicious_domains = blackbook.read().splitlines()
    blackbook.close()
    log_file = open('..\\Injector\\malicious_dns_log.txt', 'a')
    if DNS in packet and DNSQR in packet:
        src_port = packet[UDP].sport
        if src_port != DNS_PORT:
            print("Source port:", src_port)
            dns = packet[DNS]
            dns_qr = packet[DNSQR]
            if dns.qr == 0:
                print(dns_qr.qname.decode())
                for domain in malicious_domains:
                    if domain.encode() in dns_qr.qname:
                        log_line = str(get_pid(src_port)) + "," + dns_qr.qname.decode() + "\n"
                        log_file.write(log_line)
                        print("Found malicious DNS request to domain:", dns_qr.qname.decode())


def capture_dns_packets():
    host = "127.0.0.1"
    port = 5000
    client_socket = socket.socket()
    client_socket.connect((host, port))
    client_socket.send(str(getpid()).encode())
    client_socket.close()
    sniff(prn=dns_packet_callback, filter="udp port 53", store=0)


def main():
    capture_dns_packets()


if __name__ == "__main__":
    main()
