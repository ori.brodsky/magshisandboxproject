﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace dnsrequest
{
    internal class Program
    {
        static void Main()
        {
            Socket sock = new Socket(SocketType.Dgram, ProtocolType.Udp);
            IPEndPoint ep = new IPEndPoint(IPAddress.Parse("8.8.8.8"), 53);
            sock.Connect(ep);

            string host = "bbc.com";
            byte[] hostnameLength = new byte[1];
            byte[] hostdomainLength = new byte[1];


            byte[] tranactionID1 = { 0x46, 0x62 };
            byte[] queryType1 = { 0x01, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            byte[] hostname = System.Text.ASCIIEncoding.Default.GetBytes(host.Split('.')[0]);
            hostnameLength[0] = (byte)hostname.Length;
            byte[] hostdomain = System.Text.ASCIIEncoding.Default.GetBytes(host.Split('.')[1]);
            hostdomainLength[0] = (byte)hostdomain.Length;
            byte[] queryEnd = { 0x00, 0x00, 0x01, 0x00, 0x01 };
            byte[] dnsQueryString = tranactionID1.Concat(queryType1).Concat(hostnameLength).Concat(hostname).Concat(hostdomainLength).Concat(hostdomain).Concat(queryEnd).ToArray();

            Console.WriteLine("Sending dns request..");
            sock.Send(dnsQueryString);
            System.Threading.Thread.Sleep(1000);
        }

    }
}
