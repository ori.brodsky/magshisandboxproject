import socket
from os import getpid


def main():
    # Print the process ID before making the DNS request
    print("Before DNS request, Process ID:", getpid())
    # Make the DNS request
    print(socket.getaddrinfo('bbc.com', 80))

    # Print the process ID after making the DNS request
    print("After DNS request, Process ID:", getpid())

    while True:
        pass


if __name__ == "__main__":
    main()
