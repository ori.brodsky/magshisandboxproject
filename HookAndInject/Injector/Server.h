#pragma once

#include <WinSock2.h>
#include <WS2tcpip.h>
#include <Windows.h>
#include <thread>
#include <string>
#include <exception>
#include <iostream>

class Server
{
public:
	Server(int port);
	~Server();
	SOCKET accept_client();
	int serve(SOCKET clientSocket);

private:
	SOCKET _serverSocket;
};

