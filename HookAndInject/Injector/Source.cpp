#pragma comment (lib, "ws2_32.lib")

// macro to avoid problems with #include <Windows.h> and #include "WSAInitializer.h"
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>
#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <TlHelp32.h>
#include <string>
#include <cstdlib>
#include <thread>
#include <stdio.h>
#include <fstream>

#define MINUTES_TO_MS(minutes) ((minutes) * 60 * 1000)

bool stopMonitoring = false;

char dllPath[] = "HookDll.dll";
SIZE_T dllPathLen = sizeof(dllPath);
LPVOID getEntryPoint(HANDLE& hThread);
void runPythonScript();
bool strEndsWith(std::string str, std::string suffix);
void MonitorDirectory();

int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		std::cout << "Not enough arguments" << std::endl;
		return 1;
	}
	if (!strEndsWith(argv[1], ".exe"))
	{
		std::cout << "Wrong input" << std::endl;
		return 1;
	}

	std::thread monitorThread(MonitorDirectory);

	LPCSTR virusPath = argv[1];
	STARTUPINFOA startupInfo = { 0 };
	PROCESS_INFORMATION processInformation;
	startupInfo.cb = sizeof(startupInfo);
	HANDLE hProcess, hThread;
	LPVOID remoteBuffer = nullptr; // this will be the buffer of memory where we write LoadLibraryA and dll path to
	char originalEntryPoint[3] = { 0 };
	int pid;

	HMODULE hKernel32 = GetModuleHandleA("Kernel32.dll"); // this module has the LoadLibraryA function
	if (hKernel32 == NULL)
	{
		std::cerr << "Problem getting Kernel32 Module" << GetLastError() << std::endl;
		return 1;
	}

	void* loadLibrary = GetProcAddress(hKernel32, "LoadLibraryA"); // Getting LoadLibraryA address
	if (loadLibrary == NULL)
	{
		std::cerr << "Error getting LoadLibraryA address: " << GetLastError() << std::endl;
		return 1;
	}

	BOOL processCreated = CreateProcessA(virusPath, // path to target process
		NULL, // not executing any command
		NULL, // handle cannot be inherited
		NULL, // thread handle cannot be inherited
		FALSE, // handles are not inherited
		CREATE_SUSPENDED, // process's main thread is suspended upon creation, until we call ResumeThread()
		NULL, // process uses environment of calling process
		NULL, // same cd and directory of calling process
		&startupInfo, // pointer to startupinfo struct
		&processInformation); // pointer to processInformation struct

	if (!processCreated)
	{
		std::cerr << "Error creating process: " << GetLastError() << std::endl;
		return 1;
	}

	hProcess = processInformation.hProcess;
	hThread = processInformation.hThread;

	LPVOID entryPoint;
	try {
		entryPoint = getEntryPoint(hThread);
	}
	catch (const std::string& exception)
	{
		std::cerr << exception << std::endl;
		return 1;
	}


	DWORD oldProtections;
	if (!VirtualProtectEx(hProcess, entryPoint, 2, PAGE_EXECUTE_READWRITE, &oldProtections)) // making sure we can read from process entry point
	{
		std::cerr << "Error changing process permissions: " << GetLastError() << std::endl;
		return 1;
	}

	if (!ReadProcessMemory(hProcess, (LPCVOID)entryPoint, originalEntryPoint, 2, NULL)) // reading entry point first instruction
	{
		std::cerr << "Error reading entry point: " << GetLastError() << std::endl;
		return 1;
	}

	//std::string oepStr(originalEntryPoint);
	if (!WriteProcessMemory(hProcess, entryPoint, "\xEB\xFE", 2, NULL)) // writing infinite jmp loop in entry point
	{
		std::cerr << "Error writing to entry point: " << GetLastError() << std::endl;
		return 1;
	}

	ResumeThread(hThread);

	CONTEXT context;
	GetThreadContext(hThread, &context);
	/*if (!GetThreadContext(hThread, &context)) for some reason the program doesn't work when checking for this..
	{
		std::cerr << "Error getting main thread context" << GetLastError() << std::endl;
		return 1;
	}*/

	for (int i = 0; i < 50 && context.Eip != (DWORD)entryPoint; i++)
	{
		Sleep(100);

		context.ContextFlags = CONTEXT_CONTROL;
		if (!GetThreadContext(hThread, &context))
		{
			std::cerr << "Error getting main thread context" << GetLastError() << std::endl;
			return 1;
		}
	}

	// Allocate Memory in remote Process
	remoteBuffer = VirtualAllocEx(hProcess, remoteBuffer, dllPathLen + 1, (MEM_RESERVE | MEM_COMMIT), PAGE_EXECUTE_READWRITE);
	if (remoteBuffer == NULL)
	{
		std::cerr << "Problem Allocating Memory: " << GetLastError() << std::endl;
		return 1;
	}

	// Write dll path in remote Process
	if (!WriteProcessMemory(hProcess, remoteBuffer, dllPath, dllPathLen + 1, NULL))
	{
		std::cerr << "Error writing to remote process: " << GetLastError() << std::endl;
		return 1;
	}

	HANDLE remoteThread = CreateRemoteThread(hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)loadLibrary, remoteBuffer, 0, NULL);
	SuspendThread(hThread);
	WriteProcessMemory(hProcess, entryPoint, originalEntryPoint, 2, NULL);
	WaitForSingleObject(remoteThread, INFINITE); // dll has been injected
	stopMonitoring = true;

	try {
		WSAInitializer wsaInit;
		Server myServer(5000); // start listening on port 5000
		SOCKET clientSocket;
		std::thread pythonThread(runPythonScript); // run python script that connects to server
		pythonThread.detach();
		clientSocket = myServer.accept_client();
		int pythonPID = myServer.serve(clientSocket); // wait until python script responds
		closesocket(clientSocket);
		
		std::cout << "Started capturing packets..." << std::endl;
		pid = processInformation.dwProcessId;
		ResumeThread(processInformation.hThread);

		CloseHandle(processInformation.hProcess);

		WaitForSingleObject(processInformation.hThread, MINUTES_TO_MS(3));
		HANDLE pythonProc = OpenProcess(PROCESS_TERMINATE, FALSE, pythonPID);
		if (pythonProc != NULL)
		{
			if (!TerminateProcess(pythonProc, 0))
			{
				std::cerr << "Error terminating process" << std::endl;
			}
		}
		CloseHandle(pythonProc);
	}
	catch (std::exception& e)
	{
		std::cerr << "Problem with server: " << e.what() << std::endl;
		return 1;
	}

	monitorThread.join();

	std::string cmd = "python ..\\Injector\\Analyzer.py " + std::to_string(pid) + " " + argv[1];
	const char* command = cmd.c_str();
	FILE* pipe = _popen(command, "r");
	if (!pipe) {
		std::cerr << "Error opening pipe to command." << std::endl;
		return -1;
	}

	char buffer[1024];
	while (fgets(buffer, sizeof(buffer), pipe) != nullptr) {
		std::cout << buffer;
	}

	if (_pclose(pipe) == -1) {
		std::cerr << "Error closing pipe to command." << std::endl;
		return -1;
	}
	std::remove("WinAPICalls.txt");
	std::remove("malicious_dns_log.txt");
	std::remove("FilesCreated.txt");
	return 0;
}

bool strEndsWith(std::string str, std::string suffix)
{
	std::string substring = str.substr(str.length() - suffix.length());
	return substring == suffix;
}

LPVOID getEntryPoint(HANDLE& hThread)
{
	CONTEXT context;
	context.ContextFlags = CONTEXT_FULL;

	if (!GetThreadContext(hThread, &context))
		throw "Error getting thread context: " + GetLastError();

	return reinterpret_cast<LPVOID>(context.Eip);
}

void runPythonScript()
{
	system("python ..\\NetworkAnalysis\\LogDNS.py"); // run python script
}

void MonitorDirectory() {
	HANDLE directoryHandle = FindFirstChangeNotification(
		L"C:\\", //Change based on drive to search
		TRUE,
		FILE_NOTIFY_CHANGE_FILE_NAME
	);

	if (directoryHandle == INVALID_HANDLE_VALUE) {
		std::cerr << "Error setting up change notification: " << GetLastError() << std::endl;
		return;
	}

	DWORD bufferSize = 1024;
	BYTE buffer[1024];
	std::wofstream logFile("FilesCreated.txt", std::ios_base::app);

	while (!stopMonitoring) {
		if (WaitForSingleObject(directoryHandle, INFINITE) == WAIT_OBJECT_0) {

			DWORD bytesRead;
			if (ReadDirectoryChangesW(
				directoryHandle,
				buffer,
				bufferSize,
				TRUE,
				FILE_NOTIFY_CHANGE_FILE_NAME,
				&bytesRead,
				nullptr,
				nullptr)) {

				FILE_NOTIFY_INFORMATION* fileInfo = reinterpret_cast<FILE_NOTIFY_INFORMATION*>(buffer);

				while (fileInfo) {
					// Print the name of the created or modified file
					logFile << fileInfo->FileName << std::endl;

					if (fileInfo->NextEntryOffset == 0) {
						break;
					}

					fileInfo = reinterpret_cast<FILE_NOTIFY_INFORMATION*>(reinterpret_cast<BYTE*>(fileInfo) + fileInfo->NextEntryOffset);
				}
			}
			else {
				std::cerr << "Error reading directory changes: " << GetLastError() << std::endl;
				break;
			}

			// Reset the change notification
			if (FindNextChangeNotification(directoryHandle) == FALSE) {
				std::cerr << "Error resetting change notification: " << GetLastError() << std::endl;
				break;
			}
		}
		else {
			std::cerr << "Error waiting for change notification: " << GetLastError() << std::endl;
			break;
		}
	}
	logFile.close();
	FindCloseChangeNotification(directoryHandle);
}