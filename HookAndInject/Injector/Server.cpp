#include "Server.h"

Server::Server(int port)
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

	struct sockaddr_in sa = { 0 };
	std::string msg;


	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	inet_pton(AF_INET, "127.0.0.1", &sa.sin_addr.s_addr);

	std::cout << "Starting..." << std::endl;
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	std::cout << "binded\n";

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "listening..." << std::endl;

}

Server::~Server()
{
	try
	{
		closesocket(_serverSocket);
	}
	catch (...) {}
}

int Server::serve(SOCKET clientSocket)
{
	char ptr[6] = { 0 };
	int res = recv(clientSocket, ptr, 5, 0); // receiving pid from python script.
	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(clientSocket);
		throw std::exception(s.c_str());
	}
	ptr[5] = 0;
	return atoi(ptr);
	
}

SOCKET Server::accept_client()
{
	std::cout << "accepting client..." << std::endl;
	SOCKET clientSocket = accept(_serverSocket, NULL, NULL);
	if (clientSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);
	std::cout << "Client accepted !" << std::endl;
	return clientSocket;
}