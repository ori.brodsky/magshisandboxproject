import os
from sys import argv
from virustotal_scan import virustotal_analysis


class WinApiFunc:
    def __init__(self, func_call_str):
        self.name = func_call_str.split(",")[0]
        self.parameters = []
        for i in func_call_str.split(",")[1:]:
            self.parameters.append(i)

    def get_parameters(self):
        return self.parameters

    def get_name(self):
        return self.name


class MaliciousDnsRequest:
    def __init__(self, dns_log_line):
        self.pid = dns_log_line.split(",")[0]
        self.domain = dns_log_line.split(",")[1]

    def get_pid(self):
        return self.pid

    def get_domain(self):
        return self.domain


def find_attack_change_image_path(result_file, functions, virus_exe_path):
    for i in functions:
        function_name = i.get_name()
        if function_name == 'RegSetValueExA':
            reg_value_name = i.get_parameters()[3]
            reg_value = i.get_parameters()[4]
            if reg_value_name == "ImagePath" and reg_value == virus_exe_path:
                result = 'Attack Detected: attempted changing service ImagePath to virus executable.'
                print(result)
                result_file.write(result + '\n')
                return None


def is_startup_path(registry_path):
    return registry_path == 'Software\\Microsoft\\Windows\\CurrentVersion\\Run' or registry_path == 'Software\\Microsoft\\Windows\\CurrentVersion\\RunOnce'


def find_attack_autorun_program(result_file, functions, virus_exe_path):
    for i in range(len(functions[:-1])):
        curr_call = functions[i]
        if curr_call.get_name() == 'RegSetValueExA':
            regKey = curr_call.get_parameters()[0]
            subKey = curr_call.get_parameters()[1]
            filePath = curr_call.get_parameters()[4]
            if (regKey == 'HKEY_CURRENT_USER' or regKey == 'HKEY_LOCAL_MACHINE') and is_startup_path(subKey) and filePath == virus_exe_path:
                result = 'Attack Detected: program attempted adding itself to boot autorun of pc'
                print(result)
                result_file.write(result + '\n')
                return None


def find_attack_thread_hijacking(result_file, functions):
    suspend_thread_found, get_context_found, set_context_found = False, False, False
    original_eip, thread_id, proc_id = 0, 0, 0
    for func in functions:
        if not (suspend_thread_found and get_context_found and set_context_found):
            func_name = func.get_name()
            func_params = func.get_parameters()
            if func_name == "SuspendThread":
                suspend_thread_found = True
                proc_id = func_params[0]
                thread_id = func_params[1]
                calling_proc_id = func_params[2]
                if proc_id == calling_proc_id: # program suspends its own thread, not thread hijacking
                    suspend_thread_found, get_context_found, set_context_found = False, False, False
                    break
            if (func_name == "GetThreadContext" and func_params[0] == proc_id and func_params[1] == thread_id and
                    suspend_thread_found):
                original_eip = func_params[2]
                get_context_found = True
            if (func_name == "SetThreadContext" and func_params[0] == proc_id and func_params[1] == thread_id and
                    get_context_found):
                if func_params[2] != original_eip:
                    result = 'Attack Detected: program changed Eip of another process thread, likely thread hijacking'
                    print(result)
                    result_file.write(result + '\n')
                    return None


def find_attack_process_hijacking(result_file, functions):
    proc_id = -1
    for func in functions:
        if func.get_name() == "OpenProcess":
            proc_id = func.get_parameters()[1]
        if func.get_name() == "CreateRemoteThread":
            if func.get_parameters()[0] == proc_id:
                result = 'Attack Detected: program attempting to process hijack by injecting dll'
                print('Attack Detected: program attempting to process hijack by injecting dll')
                result_file.write(result + '\n')
                break


def find_attack_connect_c2(result_file, dns_requests, pid):
    for request in dns_requests:
        if request.get_pid() == pid:
            result = 'Attack Detected: program attempting to get C2 server IP. Suspicious URL: ' + request.get_domain()
            print(result)
            result_file.write('Attack Detected: program attempting to get C2 server IP. Suspicious URL:' + '\n')
            break


def find_attack_files_downloaded(result_file, functions):
    for func in functions:
        if func.get_name() == 'URLDownloadToFileA':
            file_path = os.path.abspath(func.get_parameters()[1])
            print(file_path)
            if virustotal_analysis(file_path) is not None:
                result = 'Attack Detected: program attempting to download files from Internet. Site URL: ' + func.get_parameters()[0] + ', File: ' + func.get_parameters()[1]
                print(result)
                result_file.write(result + '\n')


def find_attack_files_created(result_file, files_created, functions):
    if len(files_created) != 0:
            for i in files_created:
                result = 'Attack Detected: program attempting to create or download files. File: ' + i
                print(result)
                result_file.write(result + '\n')


def anti_vm_files_search(result_file, functions):
    vm_system_files = [
        'C:\\windows\\System32\\Drivers\\Vmmouse.sys',
        'C:\\windows\\System32\\Drivers\\vm3dgl.dll',
        'C:\\windows\\System32\\Drivers\\vmdum.dll',
        'C:\\windows\\System32\\Drivers\\vm3dver.dll',
        'C:\\windows\\System32\\Drivers\\vmtray.dll',
        'C:\\windows\\System32\\Drivers\\VMToolsHook.dll',
        'C:\\windows\\System32\\Drivers\\vmmousever.dll',
        'C:\\windows\\System32\\Drivers\\vmhgfs.dll',
        'C:\\windows\\System32\\Drivers\\vmGuestLib.dll',
        'C:\\windows\\System32\\Drivers\\VmGuestLibJava.dll',
        'C:\\windows\\System32\\Drivers\\vmhgfs.dll',
        'C:\\windows\\System32\\Drivers\\VBoxMouse.sys',
        'C:\\windows\\System32\\Drivers\\VBoxGuest.sys',
        'C:\\windows\\System32\\Drivers\\VBoxSF.sys',
        'C:\\windows\\System32\\Drivers\\VBoxVideo.sys',
        'C:\\windows\\System32\\vboxdisp.dll',
        'C:\\windows\\System32\\vboxhook.dll',
        'C:\\windows\\System32\\vboxmrxnp.dll',
        'C:\\windows\\System32\\vboxogl.dll',
        'C:\\windows\\System32\\vboxoglarrayspu.dll',
        'C:\\windows\\System32\\vboxoglcrutil.dll',
        'C:\\windows\\System32\\vboxoglerrorspu.dll',
        'C:\\windows\\System32\\vboxoglfeedbackspu.dll',
        'C:\\windows\\System32\\vboxoglpackspu.dll',
        'C:\\windows\\System32\\vboxoglpassthroughspu.dll',
        'C:\\windows\\System32\\vboxservice.exe',
        'C:\\windows\\System32\\vboxtray.exe',
        'C:\\windows\\System32\\VBoxControl.exe'
    ]
    for func in functions:
        if func.get_name() == 'PathFileExistsA' or func.get_name() == 'GetFileAttributesA':
            if func.get_parameters()[0] in vm_system_files:
                result = 'Attack Detected: program tried to Anti-VM by search of VM files'
                print(result)
                result_file.write(result + '\n')
                return True
    return False


def anti_vm_registry_keys(result_file, functions):
    vm_registry_keys = [
        'SOFTWARE\VMware, Inc.\VMware Tools',
        'SOFTWARE\Oracle\VirtualBox',
        'SOFTWARE\Microsoft\Windows NT\CurrentVersion\Virtualization',
        'SOFTWARE\Parallels\Parallels Tools',
        'SOFTWARE\Citrix\XenTools',
        'SOFTWARE\QEMU'
    ]
    for func in functions:
        if func.get_name() == 'RegOpenKeyExA':
            if func.get_parameters()[1] in vm_registry_keys or 'virtualbox' in lower(func.get_parameters()[1]) or 'vmware' in lower(func.get_parameters()[1]):
                result = 'Attack Detected: program tried to Anti-VM by search of VM registry keys files'
                print(result)
                result_file.write(result + '\n')
                return True
    return False


def find_attack_anti_vm(result_file, functions):
    anti_vm_files_search(result_file, functions)
    anti_vm_registry_keys(result_file, functions)


def find_attack_keylogger(result_file, functions):
    free_console = False
    count_get_key_state = 0
    for func in functions:
        if func.get_name() == 'FreeConsole':
            free_console = True
        if func.get_name() == 'GetAsyncKeyState':
            count_get_key_state += 1
    if count_get_key_state > 10 and free_console:
        result = "Attack Detected: KeyLogger"
        print(result)
        result_file.write(result + '\n')
        return True
    return False


def main():
    virus_path = argv[2]
    win_api_functions = []
    dns_log_lines = []
    process_id = argv[1]
    results_file = open('analyzer_results.txt', 'w')
    try:
        with open('WinAPICalls.txt', 'r') as file:
            file_contents = file.read().split("\n")
            for i in file_contents:
                win_api_functions.append(WinApiFunc(i))
            find_attack_autorun_program(results_file, win_api_functions, virus_path)
            find_attack_change_image_path(results_file, win_api_functions, virus_path)
            find_attack_process_hijacking(results_file, win_api_functions)
            find_attack_thread_hijacking(results_file, win_api_functions)
            find_attack_anti_vm(results_file, win_api_functions)
            find_attack_keylogger(results_file, win_api_functions)
    except FileNotFoundError:
        print("No suspicious WinAPI calls have been detected.")
    try:
        with open('../Injector/malicious_dns_log.txt', 'r') as file:
            file_contents = file.read().split("\n")
            for dns_line in file_contents:
                if dns_line != "":  # last line is empty
                    dns_log_lines.append(MaliciousDnsRequest(dns_line))
            find_attack_connect_c2(results_file, dns_log_lines, process_id)
    except FileNotFoundError:
        print("No suspicious dns activity has been detected.")
    try:
        with open('FilesCreated.txt', 'r') as file:
            file_contents = file.read().split("\n")
            find_attack_files_created(results_file, file_contents, win_api_functions)
    except FileNotFoundError:
        print("No files were being downloaded")
    find_attack_files_downloaded(results_file, win_api_functions)
    results_file.close()


if __name__ == "__main__":
    main()
