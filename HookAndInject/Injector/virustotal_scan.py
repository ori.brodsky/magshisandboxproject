import requests
import os
import time


def virustotal_analysis(file_path):
    url_scan = 'https://www.virustotal.com/vtapi/v2/file/scan'
    url_report = 'https://www.virustotal.com/vtapi/v2/file/report'

    api_key = '4d8ac1595898f5d988cc1748fc93a1c191cbf82537b128fe5e39ed2f8c480963'

    params = {'apikey': api_key}

    files = {'file': ('file', open(file_path, 'rb'))}

    try:
        # Step 1: Scan the file
        response_scan = requests.post(url_scan, files=files, params=params)
        #print(response_scan.json())
        # the virustotal takes about 10 seconds to analyise the file
        time.sleep(10)
        # Check if the scan request was successful
        if response_scan.status_code == 200:
            # Step 2: Retrieve the scan report
            resource = response_scan.json()['resource']
            params_report = {'apikey': api_key, 'resource': resource}
            response_report = requests.get(url_report, params=params_report)

            # Check if the report request was successful
            if response_report.status_code == 200:
                report = response_report.json()
                positives = report.get('positives', 0)

                # Print the result based on the number of positives
                if positives > 0:
                    print(f"The file is detected as malicious by {positives} antivirus engines.")
                    print("Detected by:")
                    for antivirus, result in report.get('scans', {}).items():
                        if result.get('detected'):
                            print(f"- {antivirus}: {result['result']}")
                    return positives
                else:
                    print("The file is not detected as malicious.")
            else:
                print(f"Error retrieving report: {response_report.status_code} - {response_report.text}")
        else:
            print(f"Error submitting scan request: {response_scan.status_code} - {response_scan.text}")

    except Exception as e:
        # Handle exceptions that might occur during the request
        print(f"An error occurred: {e}")
