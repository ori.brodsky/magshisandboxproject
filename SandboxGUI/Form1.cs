﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SandboxGUI
{
    public partial class Form1 : Form
    {
        TcpClient client;
        const int FILE_SIZE_LEN = 4;
        public Form1()
        {
            InitializeComponent();
            string ipAddress = "127.0.0.1";
            int port = 6000;
            client = new TcpClient();
            client.Connect(ipAddress, port);
        }

        ~Form1()
        {
            string ret = SendFile(client.GetStream(), new byte[0]);
            client.Close();
        }

        private void pbUploadFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = "C:\\";
            openFileDialog1.Title = "Select file to be upload.";
            //which type file format you want to upload in database. just add them.
            openFileDialog1.Filter = "Select Valid Document(*.exe)|*.exe";
            //FilterIndex property represents the index of the filter currently selected in the file dialog box.
            openFileDialog1.FilterIndex = 1;
            try
            {
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (openFileDialog1.CheckFileExists)
                    {
                        string path = System.IO.Path.GetFullPath(openFileDialog1.FileName);
                        lblFile.Text = openFileDialog1.SafeFileName;
                        Thread sendFileThread = new Thread(() => SendFileToServer(path));
                        sendFileThread.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void SendFileToServer(string filePath)
        {
            if (File.Exists(filePath))
            {
                static_analysis(filePath);
                byte[] content = File.ReadAllBytes(filePath);
                string response = SendFile(client.GetStream(), content);
                MessageBox.Show(response);
            }
            else
            {
                MessageBox.Show("File not found. Please enter a valid file path.");
            }

            string ret = SendFile(client.GetStream(), new byte[0]);
            client.Close();
        }

        private string SendFile(NetworkStream stream, byte[] fileBytes)
        {
            try
            {
                int fileSize = fileBytes.Length;

                // Convert file size to bytes
                byte[] fileSizeBytes = BitConverter.GetBytes(fileSize);
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(fileSizeBytes); // Ensure network byte order (big-endian)

                stream.Write(fileSizeBytes, 0, FILE_SIZE_LEN);
                stream.Write(fileBytes, 0, fileBytes.Length);

                byte[] lenBytes = new byte[FILE_SIZE_LEN];
                stream.Read(lenBytes, 0, FILE_SIZE_LEN);
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(lenBytes); // Ensure network byte order (big-endian)
                int retFileSize = BitConverter.ToInt32(lenBytes, 0);
                MessageBox.Show(retFileSize.ToString());
                byte[] retFileBytes = new byte[retFileSize];
                stream.Read(retFileBytes, 0, retFileSize);
                return Encoding.ASCII.GetString(retFileBytes);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                return "";
            }
        }

        private void static_analysis(string file_path)
        {
            string pythonScriptPath = @"F:\Magshimim\SandboxProject\magshisandboxproject\StaticAnalysis\static_analysis.py"; // change interpreter

            // Start the Python process
            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = @"C:\Users\oribr\AppData\Local\Programs\Python\Python312\python.exe";
            start.Arguments = $"{pythonScriptPath} {file_path}";
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;

            // Start the process
            using (Process process = Process.Start(start))
            {
                process.WaitForExit();
                // Read the output of the Python script
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd(); // Read the entire output
                    MessageBox.Show(result); // Display the result in a message box
                }
            }
        }
    }
}
