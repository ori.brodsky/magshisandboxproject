import socket
import struct
import subprocess
import os


def main():
    host = '192.168.1.43'
    port = 5555

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind((host, port))
    server.listen()

    print(f"[*] Listening on {host}:{port}")

    client, addr = server.accept()
    print(f"[*] Accepted connection from {addr[0]}:{addr[1]}")
    file_size_bytes = client.recv(4)
    file_size = struct.unpack('!I', file_size_bytes)[0]
    print(f"Received file size: {file_size} bytes")

    file_content = client.recv(file_size)
    script_directory = os.path.dirname(os.path.abspath(__file__))
    two_up = os.path.dirname(os.path.dirname(script_directory))
    file_name = "test.exe"
    full_path = os.path.join(two_up, "Process", file_name)
    with open(full_path, 'wb') as file:
        file.write(file_content)

    print(f"Saved file: {file_name}")
    print("Running analysis on:", file_name)
    subprocess.run(["Injector.exe", full_path])

    result_file_path = os.path.join(script_directory, "analyzer_results.txt")
    result_file_size = os.path.getsize(result_file_path)
    result_file_size_bytes = struct.pack('!I', result_file_size)
    result_file = open(result_file_path, 'rb')
    results = result_file.read()
    client.send(result_file_size_bytes)
    client.send(results)
    print("Results have been sent to server")

    client.close()


if __name__ == "__main__":
    main()
