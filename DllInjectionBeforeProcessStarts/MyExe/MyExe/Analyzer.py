import os


class WinApiFunc:
    def __init__(self, func_call_str):
        self.name = func_call_str.split(",")[0]
        self.parameters = []
        for i in func_call_str.split(",")[1:]:
            self.parameters.append(i)

    def get_parameters(self):
        return self.parameters

    def get_name(self):
        return self.name


def find_attack_delete_file(functions):
    current_directory = os.getcwd()
    parent_directory = os.path.abspath(os.path.join(current_directory, ".."))
    grandparent_directory = os.path.abspath(os.path.join(parent_directory, "..")) + "\\Virus"

    for i in functions[:1]:
        if i.get_name() == 'RemoveDirectoryA' and i.get_parameters()[0].find(grandparent_directory) == -1:
            print('Attack Detected: attempted removal of unrelated directory', i.get_parameters()[0])


def find_attack_change_image_path(functions, virus_exe_path):
    for i in functions:
        function_name = i.get_name()
        if function_name == 'RegSetValueExA':
            reg_value_name = i.get_parameters()[3]
            reg_value = i.get_parameters()[4]
            if reg_value_name == "ImagePath" and reg_value == virus_exe_path: 
                print('Attack Detected: attempted changing service ImagePath to virus executable.')


def is_startup_path(registry_path):
    return registry_path == 'Software\\Microsoft\\Windows\\CurrentVersion\\Run' or registry_path == 'Software\\Microsoft\\Windows\\CurrentVersion\\RunOnce'


def find_attack_autorun_program(functions, virus_exe_path):
    for i in range(len(functions[:-1])):
        curr_call = functions[i]
        if curr_call.get_name() == 'RegSetValueExA':
            regKey = curr_call.get_parameters()[0]
            subKey = curr_call.get_parameters()[1]
            filePath = curr_call.get_parameters()[4]
            if (regKey == 'HKEY_CURRENT_USER' or regKey == 'HKEY_LOCAL_MACHINE') and is_startup_path(subKey) and filePath == virus_exe_path:
                print('Attack Detected: program attempted adding itself to boot autorun of pc')
                return None


def find_attack_thread_hijacking(functions):
    suspend_thread_found, get_context_found, set_context_found = False, False, False
    original_eip, thread_id, proc_id = 0, 0, 0
    for func in functions:
        if not (suspend_thread_found and get_context_found and set_context_found):
            func_name = func.get_name()
            func_params = func.get_parameters()
            if func_name == "SuspendThread":
                suspend_thread_found = True
                proc_id = func_params[0]
                thread_id = func_params[1]
                calling_proc_id = func_params[2]
                if proc_id == calling_proc_id: # program suspends its' own thread, not thread hijacking
                    suspend_thread_found, get_context_found, set_context_found = False, False, False
                    break
            if (func_name == "GetThreadContext" and func_params[0] == proc_id and func_params[1] == thread_id and
                    suspend_thread_found):
                original_eip = func_params[2]
                get_context_found = True
            if (func_name == "SetThreadContext" and func_params[0] == proc_id and func_params[1] == thread_id and
                    get_context_found):
                if func_params[2] != original_eip:
                    print('Attack Detected: program changed Eip of another process thread, likely thread hijacking')
                    return None


def find_attack_process_hijacking(functions):
    proc_id = -1
    for func in functions:
        if func.get_name() == "OpenProcess":
            proc_id = func.get_parameters()[1]
        if func.get_name() == "CreateRemoteThread":
            if func.get_parameters()[0] == proc_id:
                print('Attack Detected: program attempting to process hijack by injecting dll')
                break


def main():
    virus_path = "F:\\Magshimim\\SandboxProject\\magshisandboxproject\\DllInjectionBeforeProcessStarts\\Virus\\Debug\\Virus.exe"
    win_api_functions = []
    file_path = r'D:\SandboxProject\magshisandboxproject\DllInjectionBeforeProcessStarts\MyExe\MyExe\WinAPICalls.txt'
    with open(file_path, 'r') as file:
        file_contents = file.read().split("\n")
        for i in file_contents:
            win_api_functions.append(WinApiFunc(i))

    find_attack_delete_file(win_api_functions)
    find_attack_autorun_program(win_api_functions, virus_path)
    find_attack_change_imagePath(win_api_functions, virus_path)
    find_attack_process_hijacking(win_api_functions)
    find_attack_thread_hijacking(win_api_functions)


if __name__ == "__main__":
    main()
