#include <Windows.h>
#include <iostream>
#include <TlHelp32.h>
#include <string>

char dllPath[] = "..\\..\\MyDll\\Release\\MyDll.dll";
SIZE_T dllPathLen = sizeof(dllPath);
LPVOID getEntryPoint(HANDLE& hThread);

int main()
{
	//Creating dir with "important stuff" for demonstration
	/*LPCSTR directoryPath = "C:\\Users\\idano\\Documents\\importantStuff";
	if (CreateDirectoryA(directoryPath, NULL) || GetLastError() == ERROR_ALREADY_EXISTS) {
		std::cout << "Directory created successfully or already exists." << std::endl;
	}
	else {
		std::cerr << "Error creating directory: " << GetLastError() << std::endl;
		return 0;
	}*/

	//virus for changing registries
	//LPCSTR virusPath = "..\\..\\Virus\\Debug\\Virus.exe";

	//virus for process hijacking(dll injection)
	LPCSTR virusPath = "..\\..\\ProcessHijacking\\ProcessHijacking\\Debug\\ProcessHijacking.exe";
	STARTUPINFOA startupInfo = { 0 };
	PROCESS_INFORMATION processInformation;
	startupInfo.cb = sizeof(startupInfo);
	HANDLE hProcess, hThread;
	LPVOID remoteBuffer = nullptr; // this will be the buffer of memory where we write LoadLibraryA and dll path to
	char originalEntryPoint[3] = { 0 };

	HMODULE hKernel32 = GetModuleHandleA("Kernel32.dll"); // this module has the LoadLibraryA function
	if (hKernel32 == NULL)
	{
		std::cerr << "Problem getting Kernel32 Module" << GetLastError() << std::endl;
		return 1;
	}

	void* loadLibrary = GetProcAddress(hKernel32, "LoadLibraryA"); // Getting LoadLibraryA address
	if (loadLibrary == NULL)
	{
		std::cerr << "Error getting LoadLibraryA address: " << GetLastError() << std::endl;
		return 1;
	}

	BOOL processCreated = CreateProcessA(virusPath, // path to target process
		NULL, // not executing any command
		NULL, // handle cannot be inherited
		NULL, // thread handle cannot be inherited
		FALSE, // handles are not inherited
		CREATE_SUSPENDED, // process's main thread is suspended upon creation, until we call ResumeThread()
		NULL, // process uses environment of calling process
		NULL, // same cd and directory of calling process
		&startupInfo, // pointer to startupinfo struct
		&processInformation); // pointer to processInformation struct

	if (!processCreated)
	{
		std::cerr << "Error creating process: " << GetLastError() << std::endl;
		return 1;
	}

	hProcess = processInformation.hProcess;
	hThread = processInformation.hThread;

	LPVOID entryPoint;
	try {
		entryPoint = getEntryPoint(hThread);
	}
	catch (const std::string& exception)
	{
		std::cerr << exception << std::endl;
		return 1;
	}


	DWORD oldProtections;
	if (!VirtualProtectEx(hProcess, entryPoint, 2, PAGE_EXECUTE_READWRITE, &oldProtections)) // making sure we can read from process entry point
	{
		std::cerr << "Error changing process permissions: " << GetLastError() << std::endl;
		return 1;
	}

	if (!ReadProcessMemory(hProcess, (LPCVOID)entryPoint, originalEntryPoint, 2, NULL)) // reading entry point first instruction
	{
		std::cerr << "Error reading entry point: " << GetLastError() << std::endl;
		return 1;
	}

	//std::string oepStr(originalEntryPoint);
	if (!WriteProcessMemory(hProcess, entryPoint, "\xEB\xFE", 2, NULL)) // writing infinite jmp loop in entry point
	{
		std::cerr << "Error writing to entry point: " << GetLastError() << std::endl;
		return 1;
	}

	ResumeThread(hThread);

	CONTEXT context;
	GetThreadContext(hThread, &context);
	/*if (!GetThreadContext(hThread, &context)) for some reason the program doesn't work when checking for this..
	{
		std::cerr << "Error getting main thread context" << GetLastError() << std::endl;
		return 1;
	}*/

	for (int i = 0; i < 50 && context.Eip != (DWORD)entryPoint; i++)
	{
		Sleep(100);

		context.ContextFlags = CONTEXT_CONTROL;
		if (!GetThreadContext(hThread, &context))
		{
			std::cerr << "Error getting main thread context" << GetLastError() << std::endl;
			return 1;
		}
	}

	// Allocate Memory in remote Process
	remoteBuffer = VirtualAllocEx(hProcess, remoteBuffer, dllPathLen + 1, (MEM_RESERVE | MEM_COMMIT), PAGE_EXECUTE_READWRITE);
	if (remoteBuffer == NULL)
	{
		std::cerr << "Problem Allocating Memory: " << GetLastError() << std::endl;
		return 1;
	}

	// Write dll path in remote Process
	if (!WriteProcessMemory(hProcess, remoteBuffer, dllPath, dllPathLen + 1, NULL))
	{
		std::cerr << "Error writing to remote process: " << GetLastError() << std::endl;
		return 1;
	}

	HANDLE remoteThread = CreateRemoteThread(hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)loadLibrary, remoteBuffer, 0, NULL);
	SuspendThread(hThread);
	WriteProcessMemory(hProcess, entryPoint, originalEntryPoint, 2, NULL);
	WaitForSingleObject(remoteThread, INFINITE);
	//Sleep(100);

	ResumeThread(processInformation.hThread);

	CloseHandle(processInformation.hProcess);




	return 0;
}

LPVOID getEntryPoint(HANDLE& hThread)
{
	CONTEXT context;
	context.ContextFlags = CONTEXT_FULL;

	if (!GetThreadContext(hThread, &context))
		throw "Error getting thread context: " + GetLastError();

	return reinterpret_cast<LPVOID>(context.Eip);
}