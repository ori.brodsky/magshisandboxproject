from peid_scan import peid_analysis
from virustotal_scan import virustotal_analysis
from yara_rules_scan import yara_rules_analysis


def main():
    print("PEiD scan report:")
    peid_results = peid_analysis("C:\\Users\\idano\\Downloads\\upx_Autoruns.exe").decode()
    print(peid_results)
    print("Yara rules static analysis report:")
    yara_rules_analysis(".\\malware", "C:\\Users\\idano\\Downloads\\upx_Autoruns.exe")
    print("Virus total scan report:")
    virustotal_analysis()


if __name__ == '__main__':
    main()

