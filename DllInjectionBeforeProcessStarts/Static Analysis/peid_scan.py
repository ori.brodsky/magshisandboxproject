from subprocess import check_output

def peid_analysis(file_path):
    command = "peid " + file_path
    return check_output(command)
