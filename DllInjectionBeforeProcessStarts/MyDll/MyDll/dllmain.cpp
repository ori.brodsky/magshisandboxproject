// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include <Windows.h>
#include <iostream>
#include <fstream>
#include <map>

typedef struct regKey {
	std::string hKey;
	std::string lpSubKey;
	std::string samDesired;
} regKey;

enum hookedFunctions{REMOVE_DIRECTORY_A, REG_OPEN_KEY_EX_A, REG_SET_VALUE_EX_A, REG_CREATE_KEY_EX_A, OPEN_PROCESS, CREATE_REMOTE_THREAD, SUSPEND_THREAD, GET_THREAD_CONTEXT, SET_THREAD_CONTEXT};
char saved_buffer[10][5] = { 0 }; // buffer to save original bytes so we can execute function as normally after executing our code.
FARPROC hooked_address[10] = { NULL };
std::map<HKEY, regKey> hkey_map;
std::map<HANDLE, int> processes_map;

void install_hook_generic(LPCSTR function, LPCSTR libraryName, VOID* functionOverrider, int hookedFunctionIndex);


BOOL WINAPI myRemoveDirectoryA(LPCSTR lpPathName)
{
	std::ofstream logFile("WinAPICalls.txt", std::ios_base::app);
	logFile << "RemoveDirectoryA," << lpPathName << std::endl;
	logFile.close();

	std::cout << "Intercepted delete directory" << std::endl;
	WriteProcessMemory(GetCurrentProcess(), (LPVOID)hooked_address[REMOVE_DIRECTORY_A], saved_buffer[REMOVE_DIRECTORY_A], 5, NULL);

	BOOL retValue = RemoveDirectoryA(lpPathName); // call original function with my data
	install_hook_generic("RemoveDirectoryA", "Kernel32.dll", &myRemoveDirectoryA, REMOVE_DIRECTORY_A);
	return retValue;
}

std::string hKeyToStr(HKEY hKey)
{
	std::string hKeyString;
	if (hKey == HKEY_CURRENT_USER) hKeyString = "HKEY_CURRENT_USER";
	if (hKey == HKEY_CLASSES_ROOT) hKeyString = "HKEY_CLASSES_ROOT";
	if (hKey == HKEY_LOCAL_MACHINE) hKeyString = "HKEY_LOCAL_MACHINE";
	if (hKey == HKEY_USERS) hKeyString = "HKEY_USERS";
	if (hKey == HKEY_CURRENT_CONFIG) hKeyString = "HKEY_CURRENT_CONFIG";
	return hKeyString;
}

std::string samToStr(REGSAM samDesired)
{
	std::string samDesiredString;
	if (samDesired == KEY_ALL_ACCESS) samDesiredString = "KEY_ALL_ACCESS";
	if (samDesired == KEY_CREATE_LINK) samDesiredString = "KEY_CREATE_LINK";
	if (samDesired == KEY_CREATE_SUB_KEY) samDesiredString = "KEY_CREATE_SUB_KEY";
	if (samDesired == KEY_ENUMERATE_SUB_KEYS) samDesiredString = "KEY_ENUMERATE_SUB_KEYS";
	if (samDesired == KEY_EXECUTE) samDesiredString = "KEY_EXECUTE";
	if (samDesired == KEY_NOTIFY) samDesiredString = "KEY_NOTIFY";
	if (samDesired == KEY_QUERY_VALUE) samDesiredString = "KEY_QUERY_VALUE";
	if (samDesired == KEY_READ) samDesiredString = "KEY_READ";
	if (samDesired == KEY_SET_VALUE) samDesiredString = "KEY_SET_VALUE";
	if (samDesired == KEY_WRITE) samDesiredString = "KEY_WRITE";
	return samDesiredString;
}

std::string desiredAccessToStr(DWORD desiredAccess)
{
	std::string str;
	if (desiredAccess == PROCESS_CREATE_PROCESS) str = "PROCESS_CREATE_PROCESS";
	else if (desiredAccess == PROCESS_CREATE_THREAD) str = "PROCESS_CREATE_THREAD";
	else if (desiredAccess == PROCESS_DUP_HANDLE) str = "PROCESS_DUP_HANDLE";
	else if (desiredAccess == PROCESS_QUERY_INFORMATION) str = "PROCESS_QUERY_INFORMATION";
	else if (desiredAccess == PROCESS_QUERY_LIMITED_INFORMATION) str = "PROCESS_QUERY_LIMITED_INFORMATION";
	else if (desiredAccess == PROCESS_SET_INFORMATION) str = "PROCESS_SET_INFORMATION";
	else if (desiredAccess == PROCESS_SET_QUOTA) str = "PROCESS_SET_QUOTA";
	else if (desiredAccess == PROCESS_SUSPEND_RESUME) str = "PROCESS_SUSPEND_RESUME";
	else if (desiredAccess == PROCESS_TERMINATE) str = "PROCESS_TERMINATE";
	else if (desiredAccess == PROCESS_VM_OPERATION) str = "PROCESS_VM_OPERATION";
	else if (desiredAccess == PROCESS_VM_READ) str = "PROCESS_VM_READ";
	else if (desiredAccess == PROCESS_VM_WRITE) str = "PROCESS_VM_WRITE";
	else str = "ACCESS_RIGHT";
	return str;
}

LSTATUS WINAPI myRegCreateKeyExA(HKEY hKey, LPCSTR lpSubKey, DWORD Reserved, LPSTR lpClass, DWORD dwOptions, REGSAM samDesired, const LPSECURITY_ATTRIBUTES lpSecurityAttributes, PHKEY phkResult, LPDWORD lpdwDisposition)
{
	std::ofstream logFile("WinAPICalls.txt", std::ios_base::app);
	std::string hKeyString = hKeyToStr(hKey), samDesiredString = samToStr(samDesired);

	regKey currRegKey;
	currRegKey.hKey = hKeyString;
	currRegKey.lpSubKey = lpSubKey;
	currRegKey.samDesired = samDesiredString;

	logFile << "RegCreateKeyExA," << hKeyString << "," << lpSubKey << "," << samDesiredString << std::endl;
	logFile.close();

	WriteProcessMemory(GetCurrentProcess(), (LPVOID)hooked_address[REG_CREATE_KEY_EX_A], saved_buffer[REG_CREATE_KEY_EX_A], 5, NULL);
	LSTATUS returnValue = RegCreateKeyExA(hKey, lpSubKey, Reserved, lpClass, dwOptions, samDesired, lpSecurityAttributes, phkResult, lpdwDisposition); // call original function with my data
	hkey_map[*phkResult] = currRegKey;
	install_hook_generic("RegCreateKeyExA", "Advapi32.dll", &myRegCreateKeyExA, REG_CREATE_KEY_EX_A);
	return returnValue;
}

LSTATUS WINAPI myRegOpenKeyExA(HKEY hKey,LPCSTR lpSubKey, DWORD ulOptions, REGSAM samDesired, PHKEY phkResult)
{
	std::ofstream logFile("WinAPICalls.txt", std::ios_base::app);
	std::string hKeyString = hKeyToStr(hKey), samDesiredString = samToStr(samDesired);
	
	regKey currRegKey;
	currRegKey.hKey = hKeyString;
	currRegKey.lpSubKey = lpSubKey;
	currRegKey.samDesired = samDesiredString;

	logFile << "RegOpenKeyExA," << hKeyString << "," << lpSubKey << "," << samDesiredString << std::endl;
	logFile.close();

	WriteProcessMemory(GetCurrentProcess(), (LPVOID)hooked_address[REG_OPEN_KEY_EX_A], saved_buffer[REG_OPEN_KEY_EX_A], 5, NULL);

	LSTATUS returnValue = RegOpenKeyExA(hKey, lpSubKey, ulOptions, samDesired, phkResult); // call original function with my data
	hkey_map[*phkResult] = currRegKey;
	install_hook_generic("RegOpenKeyExA", "Advapi32.dll", &myRegOpenKeyExA, REG_OPEN_KEY_EX_A);
	return returnValue;
}

LSTATUS WINAPI myRegSetValueExA(HKEY hKey, LPCSTR lpValueName, DWORD Reserved, DWORD dwType, const BYTE* lpData, DWORD cbData)
{
	std::ofstream logFile("WinAPICalls.txt", std::ios_base::app);
	regKey currKey = hkey_map[hKey];
	logFile << "RegSetValueExA," << currKey.hKey << "," << currKey.lpSubKey << "," << currKey.samDesired << "," << lpValueName << "," << lpData << std::endl;
	logFile.close();

	WriteProcessMemory(GetCurrentProcess(), (LPVOID)hooked_address[REG_SET_VALUE_EX_A], saved_buffer[REG_SET_VALUE_EX_A], 5, NULL);

	LSTATUS retValue = RegSetValueExA(hKey, lpValueName, Reserved, dwType, lpData, cbData);
	install_hook_generic("RegSetValueExA", "Advapi32.dll", &myRegSetValueExA, REG_SET_VALUE_EX_A);
	return retValue;
}

HANDLE WINAPI myOpenProcess(DWORD dwDesiredAccess, BOOL  bInheritHandle, DWORD dwProcessId)
{
	std::ofstream logFile("WinAPICalls.txt", std::ios_base::app);
	logFile << "OpenProcess," << desiredAccessToStr(dwDesiredAccess) << "," << dwProcessId << std::endl;
	logFile.close();

	WriteProcessMemory(GetCurrentProcess(), (LPVOID)hooked_address[OPEN_PROCESS], saved_buffer[OPEN_PROCESS], 5, NULL);
	HANDLE retValue = OpenProcess(dwDesiredAccess, bInheritHandle, dwProcessId);
	processes_map[retValue] = dwProcessId;
	install_hook_generic("OpenProcess", "Kernel32.dll", &myOpenProcess, OPEN_PROCESS);
	return retValue;
}

HANDLE WINAPI myCreateRemoteThread(HANDLE hProcess, LPSECURITY_ATTRIBUTES lpThreadAttributes, SIZE_T dwStackSize, LPTHREAD_START_ROUTINE lpStartAddress, LPVOID lpParameter, DWORD dwCreationFlags, LPDWORD lpThreadId)
{
	std::ofstream logFile("WinAPICalls.txt", std::ios_base::app);
	logFile << "CreateRemoteThread," << processes_map[hProcess] << std::endl;
	logFile.close();
	WriteProcessMemory(GetCurrentProcess(), (LPVOID)hooked_address[CREATE_REMOTE_THREAD], saved_buffer[CREATE_REMOTE_THREAD], 5, NULL);
	HANDLE retValue = CreateRemoteThread(hProcess, lpThreadAttributes, dwStackSize, lpStartAddress, lpParameter, dwCreationFlags, lpThreadId);
	install_hook_generic("CreateRemoteThread", "Kernel32.dll", &myCreateRemoteThread, CREATE_REMOTE_THREAD);
}

/*
Function writes process id and thread id to log file in the following format:
SuspendThread,processId,threadId,callingProcessId
The function then calls the original SuspendThread and returns its' value.
*/
DWORD WINAPI mySuspendThread(HANDLE hThread)
{
	DWORD threadId = GetThreadId(hThread);
	DWORD processId = GetProcessIdOfThread(hThread);
	std::ofstream logFile("WinAPICalls.txt", std::ios_base::app);
	logFile << "SuspendThread," << processId << "," << threadId << "," << GetCurrentProcessId() << std::endl;
	logFile.close();

	WriteProcessMemory(GetCurrentProcess(), (LPVOID)hooked_address[SUSPEND_THREAD], saved_buffer[SUSPEND_THREAD], 5, NULL);

	DWORD retValue = SuspendThread(hThread);
	install_hook_generic("SuspendThread", "Kernel32.dll", &mySuspendThread, SUSPEND_THREAD);
	return retValue;
}

/*
Function writes process id, thread id and instruction pointer value to log file in the following format:
GetThreadContext,processId,threadId,Eip
The function then calls the original GetThreadContext and returns its' value.
*/
BOOL WINAPI myGetThreadContext(HANDLE hThread, LPCONTEXT lpContext)
{
	DWORD threadId = GetThreadId(hThread);
	DWORD processId = GetProcessIdOfThread(hThread);
	std::ofstream logFile("WinAPICalls.txt", std::ios_base::app);
	WriteProcessMemory(GetCurrentProcess(), (LPVOID)hooked_address[GET_THREAD_CONTEXT], saved_buffer[GET_THREAD_CONTEXT], 5, NULL);
	DWORD retValue = GetThreadContext(hThread, lpContext);
	logFile << "GetThreadContext," << processId << "," << threadId << "," << lpContext->Eip << std::endl;
	logFile.close();


	install_hook_generic("GetThreadContext", "Kernel32.dll", &myGetThreadContext, GET_THREAD_CONTEXT);
	return retValue;
}

/*
Function writes process id, thread id and instruction pointer value to log file in the following format:
SetThreadContext,processId,threadId,Eip
The function then calls the original SetThreadContext and returns its' value.
*/
BOOL WINAPI mySetThreadContext(HANDLE hThread, LPCONTEXT lpContext)
{
	DWORD threadId = GetThreadId(hThread);
	DWORD processId = GetProcessIdOfThread(hThread);
	std::ofstream logFile("WinAPICalls.txt", std::ios_base::app);
	logFile << "SetThreadContext," << processId << "," << threadId << "," << lpContext->Eip << std::endl;
	logFile.close();

	WriteProcessMemory(GetCurrentProcess(), (LPVOID)hooked_address[SET_THREAD_CONTEXT], saved_buffer[SET_THREAD_CONTEXT], 5, NULL);

	DWORD retValue = SetThreadContext(hThread, lpContext);
	install_hook_generic("SetThreadContext", "Kernel32.dll", &mySetThreadContext, SET_THREAD_CONTEXT);
	return retValue;
}

void install_hook_generic(LPCSTR function, LPCSTR libraryName, VOID* functionOverrider, int hookedFunctionIndex)
{
	HINSTANCE hinstLib;
	VOID* myFuncAddress;
	DWORD* relative_offset;
	DWORD src;
	DWORD dst;
	CHAR patch[5] = { 0 }; // jmp instruction will consist of 5 bytes, 1st byte is opcode and 4 bytes of the address to jump to

	hinstLib = LoadLibraryA(libraryName);
	if (hinstLib == NULL)
	{
		std::cerr << "Couldn't find dll" << std::endl;
		return;
	}

	hooked_address[hookedFunctionIndex] = GetProcAddress(hinstLib, function);

	// save original 5 bytes
	BOOL isSuccess = ReadProcessMemory(GetCurrentProcess() // handle to process to read from
		, (LPCVOID)hooked_address[hookedFunctionIndex] // address to read from (we want to read first 5 bytes from function address)
		, saved_buffer[hookedFunctionIndex] // buffer to save memory read
		, 5 // amount of bytes to read
		, NULL); // pointer to variable holding number of bytes read
	if (!isSuccess)
	{
		std::cerr << "Error reading memory from current process" << std::endl;
		std::cout << GetLastError();
		return;
	}

	myFuncAddress = functionOverrider;
	// calculating how many bytes to jump
	src = (DWORD)hooked_address[hookedFunctionIndex] + 5;
	dst = (DWORD)myFuncAddress;
	relative_offset = (DWORD*)(dst - src);

	memcpy(patch, "\xE9", 1); // first byte is jmp opcode (e9)
	memcpy(patch + 1, &relative_offset, 4); // 4 bytes is the address to jump to

	isSuccess = WriteProcessMemory(GetCurrentProcess(), (LPVOID)hooked_address[hookedFunctionIndex], patch, 5, NULL); // override first 5 bytes to perform our jump instruction
	if (!isSuccess)
	{
		std::cerr << "Error writing memory to current process" << std::endl;
		std::cout << GetLastError();
		return;
	}
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        std::cout << "Hello from DLL" << std::endl;
		install_hook_generic("RemoveDirectoryA", "Kernel32.dll", &myRemoveDirectoryA, REMOVE_DIRECTORY_A);
		install_hook_generic("RegOpenKeyExA", "Advapi32.dll", &myRegOpenKeyExA, REG_OPEN_KEY_EX_A);
		install_hook_generic("RegSetValueExA", "Advapi32.dll", &myRegSetValueExA, REG_SET_VALUE_EX_A);
		install_hook_generic("RegCreateKeyExA", "Advapi32.dll", &myRegCreateKeyExA, REG_CREATE_KEY_EX_A);
		install_hook_generic("OpenProcess", "Kernel32.dll", &myOpenProcess, OPEN_PROCESS);
		install_hook_generic("CreateRemoteThread", "Kernel32.dll", &myCreateRemoteThread, CREATE_REMOTE_THREAD);
		install_hook_generic("SuspendThread", "Kernel32.dll", &mySuspendThread, SUSPEND_THREAD);
		install_hook_generic("GetThreadContext", "Kernel32.dll", &myGetThreadContext, GET_THREAD_CONTEXT);
		install_hook_generic("SetThreadContext", "Kernel32.dll", &mySetThreadContext, SET_THREAD_CONTEXT);
        break;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

