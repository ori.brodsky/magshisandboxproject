import yara
import os


def yara_rules_analysis(rules_directory, file_to_scan):
    #rules_directory = 'C:\\Users\\vboxuser\\Desktop\\Static Analysis\\malware'  # Change to your actual directory path

    #file_to_scan = 'C:\\Users\\vboxuser\\Desktop\\Ransomware.WannaCry\\ed01ebfbc9eb5bbea545af4d01bf5f1071661840480439c6e5babe8e080e41aa.exe'  # Change to your actual file path

    all_matches = []

    for rule_file in os.listdir(rules_directory):
        if rule_file.endswith('.yar'):
            rule_file_path = os.path.join(rules_directory, rule_file)

            try:
                rules = yara.compile(rule_file_path)
            except:
                print("Problem with yara rule;", rule_file_path)

            matches = rules.match(file_to_scan)

            all_matches.extend(matches)

    if all_matches:
        print("Matches found:")
        for match in all_matches:
            print(f"Rule: {match.rule}, Strings: {match.strings}")
    else:
        print("No matches found.")
