from peid_scan import peid_analysis
from virustotal_scan import virustotal_analysis
#from yara_rules_scan import yara_rules_analysis
import sys

def analyze_file(file_path):
    print("PEiD scan report:")
    peid_results = peid_analysis(file_path).decode()
    print(peid_results)
    print("Yara rules static analysis report:")
    yara_rules_analysis(".\\malware", file_path)
    print("Virus total scan report:")
    virustotal_analysis(file_path)

def main():
    args = sys.argv[1:]
    if len(args) == 0:
        print("Not enough arguments")
        return
    analyze_file(args[0])

if __name__ == '__main__':
    main()
