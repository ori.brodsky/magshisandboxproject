import socket
import os
import struct

FILE_SIZE_LEN = 4


def send_file(client_socket, file_size, file_content):

    # Send the file size
    file_size_bytes = struct.pack('!I', file_size)
    client_socket.send(file_size_bytes)
    print(f"Sent file size: {file_size} bytes")

    # Send the file content
    client_socket.send(file_content)

    results_file_size_bytes = client.recv(FILE_SIZE_LEN)
    results_file_size = struct.unpack('!I', results_file_size_bytes)[0]
    results = client.recv(results_file_size)
    print(results.decode())
    file_name = "final_results.txt"
    with open(file_name, 'wb') as results_file:
        results_file.write(results)


if __name__ == "__main__":
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(("127.0.0.1", 6000))
    path = input("Enter the path of the .exe file to send: ")
    while path != "exit":
        size = os.path.getsize(path)
        if size == 0:
            print("Can't send an empty file. Try again.")
            continue
        content = ""
        with open(path, 'rb') as file:
            content = file.read()
        send_file(client, size, content)
        path = input("Enter the path of the .exe file to send: ")
    send_file(client, 0, "".encode())
    # Close the connection
    client.close()
