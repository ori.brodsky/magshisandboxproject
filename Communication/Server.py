import socket
import threading
import struct
#from ClientManager import VirtualMachine
from queue import Queue
import time
import os

FILE_SIZE_LEN = 4


clients = {}
file_queue = Queue()
#vm_list = [VirtualMachine("Windows 10", "Waiting for file sent", "192.168.1.40")]#,
           #VirtualMachine("Windows 10 Clone 1", "Waiting for connection", "192.168.1.42")]
file_queue_lock = threading.Lock()
client_dict_lock = threading.Lock()


class FileData:
    def __init__(self, client_address, file_size, file_content):
        self.client_address = client_address
        self.file_size = file_size
        self.file_content = file_content

    def __str__(self):
        return "Client: " + self.client_address + "\n" + "File size: " + str(self.file_size) + "\n"


def handle_client(client_address):
    with client_dict_lock:
        client_socket = clients[client_address]
    while True:
        # Receive the file size
        file_size_bytes = client_socket.recv(FILE_SIZE_LEN)
        file_size = struct.unpack('!I', file_size_bytes)[0]

        if file_size == 0:  # means client exited application and sent file size 0
            break
        print(f"Received file size: {file_size} bytes")

        # Receive the file content
        received_size = 0
        file_content = b""
        while received_size < file_size:
            file_chunk = client_socket.recv(file_size - received_size)
            if file_chunk == 0:  # means we received the entire file
                break
            received_size += len(file_chunk)
            file_content += file_chunk
        file_data = FileData(client_address, file_size, file_content)

        file_size = os.path.getsize("final_results.txt")
        file_size_bytes = struct.pack('!I', file_size)
        client_socket.send(file_size_bytes)
        print(f"Sent file size: {file_size} bytes")
        with open("final_results.txt", 'rb') as file:
            file_content = file.read()
            client_socket.send(file_content)
        # with file_queue_lock:
        #     file_queue.put(file_data)

    # Close the connection
    client_socket.close()


# def handle_vm(machine):
#     while True:
#         with file_queue_lock:
#             if not file_queue.empty():
#                 file_data = file_queue.get()
#             else:
#                 time.sleep(1)  # wait until queue has file to process
#                 continue

#         machine.restore()
#         machine.launch()
#         time.sleep(5)
#         vm_client_sock = machine.vm_send_file(file_data.file_size, file_data.file_content)
#         print("Waiting for results.")
#         results_file_size_bytes = vm_client_sock.recv(FILE_SIZE_LEN)
#         results_file_size = struct.unpack('!I', results_file_size_bytes)[0]
#         results = vm_client_sock.recv(results_file_size)
#         print("Server received results, sending to client")

#         machine.shutdown()
#         time.sleep(3)  # wait for complete shutdown
#         client_socket = clients[file_data.client_address]
#         client_socket.send(results_file_size_bytes)
#         client_socket.send(results)
#         vm_client_sock.close()


def start_server():
    host = '127.0.0.1'
    port = 6000

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind((host, port))
    server.listen(5)

    print(f"[*] Listening on {host}:{port}")

    #vm_threads = [threading.Thread(target=handle_vm, args=(machine, )) for machine in vm_list]
    #for thread in vm_threads:
    #    thread.start()

    while True:
        client, addr = server.accept()
        print(f"[*] Accepted connection from {addr[0]}:{addr[1]}")
        clients[addr[0]] = client
        print(clients)

        client_handler = threading.Thread(target=handle_client, args=(addr[0],))
        client_handler.start()


if __name__ == "__main__":
    start_server()
