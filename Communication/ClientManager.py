import vboxapi
from Client import send_file
import socket
import struct

VM_SERVER_PORT = 5555
FILE_SIZE_LEN = 4


class VirtualMachine:
    vbox_manager = vboxapi.VirtualBoxManager()

    def __init__(self, vm_name, snap_name, ip_addr):
        self.ip_addr = ip_addr
        self.vm_name = vm_name
        self.snap_name = snap_name
        self.vbox = self.vbox_manager.getVirtualBox()
        self.vm = self.vbox.findMachine(vm_name)
        self.session = self.vbox_manager.getSessionObject(self.vbox)

    def launch(self):
        self.vm = self.vbox.findMachine(self.vm_name)
        self.session = self.vbox_manager.getSessionObject(self.vbox)
        progress = self.vm.launchVMProcess(self.session, "gui", [])
        progress.waitForCompletion(-1)

    def shutdown(self):
        console = self.session.console
        progress = console.powerDown()
        progress.waitForCompletion(-1)

    def restore(self):
        self.vm.lockMachine(self.session, self.vbox_manager.constants.LockType_Shared)

        self.session.unlockMachine()

        session = self.vbox_manager.openMachineSession(self.vm)  # WHY?
        vm = session.machine
        snap = vm.restoreSnapshot(vm.findSnapshot(self.snap_name))
        snap.waitForCompletion(-1)
        session.unlockMachine()

    def vm_send_file(self, file_size, file_content):
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((self.ip_addr, VM_SERVER_PORT))

        # Send the file size
        file_size_bytes = struct.pack('!I', file_size)
        client.send(file_size_bytes)
        print(f"Sent file size: {file_size} bytes")

        # Send the file content
        client.send(file_content)

        return client


def main():
    pass
    #machine = VirtualMachine("Windows 10", "Waiting for file")
    #machine.restore()
    #machine.launch()


if __name__ == "__main__":
    main()
